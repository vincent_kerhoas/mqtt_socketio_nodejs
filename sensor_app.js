var mqtt = require('mqtt');
const imu = require("node-sense-hat").Imu;
const IMU = new imu.IMU();
var client = mqtt.connect('mqtt://192.168.0.2');
var Gpio = require('onoff').Gpio; 
var LED = new Gpio(4, 'out'); 
const nb_sleep = require('sleep-async')();

//======================================================================
client.on('connect', () => {
   client.subscribe('sense_hat/sensors/nbr_meas');
   client.subscribe('sense_hat/gpio/switch_led');
})
//======================================================================
client.on('message', function (topic, payload, packet) {
	console.log( topic+'='+ payload);
	switch (topic) {
    case 'sense_hat/sensors/nbr_meas':
      return handleSensorsNbr_meas(payload)
    case 'sense_hat/gpio/switch_led':
      return handleGpioSwitch_led(payload)  
  }
});
//======================================================================
function handleGpioSwitch_led(payload)  {
	var ledS="OFF";
	console.log('SWITCH LED %s', payload)
	    if( payload == 'ON' ) 
		{
			console.log("query true");
			LED.writeSync(1);
			ledS="LEDS_ON";
		}	
		else	
		{
			console.log("query false");
			LED.writeSync(0);
			ledS="LEDS_OFF";
		}	
	client.publish('sense_hat/gpio/led_state',  ledS.toString()  )				  
}	
//======================================================================
function handleSensorsNbr_meas(payload) {
  console.log('Nbr_meas %s', payload)

  var it = 0; 
  it = parseInt(payload);
	if (it) {
			  console.log(it); 
			  var sleep = require('system-sleep');
			  for(var i=0 ; i < it ; i++)
			  {
					IMU.getValue((err, data) => {
						if (err !== null) {
								console.error("Could not read sensor data: ", err);
								return;
						 }
						console.log("Accelleration is: ", JSON.stringify(data.accel, null, "  "));
						console.log("Gyroscope is: ", JSON.stringify(data.gyro, null, "  "));
						console.log("Compass is: ", JSON.stringify(data.compass, null, "  "));
						console.log("Fusion data is: ", JSON.stringify(data.fusionPose, null, "  "));

						console.log("Temp is: ", data.temperature);
						console.log("Pressure is: ", data.pressure);
						console.log("Humidity is: ", data.humidity);
													  		
						client.publish('sense_hat/sensors/pressure',  Number(data.pressure.toFixed(2)).toString() )				    
						});

					sleep(1000);

			   }			
			}
}
//======================================================================
process.on('SIGINT', function () { 	//on ctrl+c
  LED.writeSync(0); 				// Turn LED off
  LED.unexport(); 					// Unexport LED GPIO to free resources
  process.exit(); 					//exit completely
}); 
//======================================================================	

			
			

	
	
				
	
	
	




