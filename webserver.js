express = require('express');
var app = express();
var http = require('http').Server(app);
var fs = require('fs'); 
var io = require('socket.io')(http) 
var url = require('url'); 
var mqtt = require('mqtt');
//======================================================================
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.static(__dirname + '/www'))
//======================================================================
http.listen(8080); //listen to port 8080
var mqtt_client = mqtt.connect('mqtt://192.168.0.2');
//======================================================================
//					SERVE INDEX
//======================================================================
app.get("/", function(req, res) {
	res.setHeader('Content-type', 'text/xml');
    res.sendFile('index.html');
})
//======================================================================
app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})
//======================================================================
io.sockets.on('connect', function (socket) { // WebSocket Connection	
			//##########################################################
			// 			BRIDGE WEBSOCKETIO --> MQTT BROKER 
			//##########################################################
			  socket.on('subscribe', function(data){
				 console.log('subscribing to' + data.topic );
				 mqtt_client.subscribe(data.topic);
			  });

			  socket.on('publish', function(data) {
					console.log('publishing to ' + data.topic);
					mqtt_client.publish(data.topic, data.payload);
			  });
	  			  			  
			//##########################################################
			// 			BRIDGE MQTT BROKER --> WEBSOCKETIO 
			//##########################################################
			mqtt_client.on('message', function (topic, payload, packet) {
				socket.emit('mqtt',{'topic': String(topic), 'payload': String(payload)});	
				console.log( topic+'='+ payload);
			});	  
}); 
//======================================================================
